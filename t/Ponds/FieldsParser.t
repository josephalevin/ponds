use strict;
use warnings;

use Test::More;

# Verify module can be included via "use" pragma
BEGIN { use_ok('Ponds::FieldsParser') };

# Verify module can be included via "require" pragma
require_ok( 'Ponds::FieldsParser' );

require Ponds::FieldsParser;

my $meta = {
    testing => 'just a name here',
    name => {
        type => 'string',
    },
    phone => {
        type => 'phone',
    },
    details=> {
        age => {
            type => 'int',
        }
    }
};

is_deeply(
    Ponds::FieldsParser::to_template( {meta=>$meta} ), 
    {
        name => 1,
        phone => 1,
        details => {
            age =>1,
        },
    }, 
    'to_template'
);

is_deeply(
    Ponds::FieldsParser::filter({
        template=>{
            name=>1,
            details=>{
                age =>1,
            },
            arrays=>{
                id=>1,
            }
        },
        result=>{
            name=>'joseph',
            height=>183,
            details=>{
                age=>29,
                postal=>'123',
            },
            arrays => [
                {
                    id=>42,
                    name=>'test',
                },
                {
                    id=>13,
                    name=>'testb',
                },
            ],
        }
    }),
    {
        name => 'joseph',
        details=>{
            age=>29,
        },
        arrays => [
            { id=>42, },
            { id=>13, },
        ],
    },
    'filter'
);


done_testing();
