package Ponds;

use strict;
use warnings;

use Ponds::Context qw( :all );

our (@ISA, @EXPORT_OK, %EXPORT_TAGS);
BEGIN { 
    require Exporter;
    @ISA = qw( Exporter );
    @EXPORT_OK = @{$Ponds::Context::EXPORT_TAGS{all}};
    %EXPORT_TAGS = (
        context => $Ponds::Context::EXPORT_TAGS{all},
    );
}

