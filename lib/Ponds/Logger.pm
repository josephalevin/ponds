package Ponds::Logger;

use strict;
use warnings;

use Moo;
use Ponds qw( :context );

sub _log {
    my ($self, $level, $message, @args) = @_;
    my $logger = env->{'psgix.logger'};
    return unless $logger;
    $message = sprintf($message, @args);
    $logger->({
        level        => $level, 
        message      => $message, 
        caller_depth => 3,
    });
}

sub error { shift->_log('error', @_) }
sub warn  { shift->_log('warn', @_) }
sub info  { shift->_log('info', @_) }
sub debug { shift->_log('debug', @_) }
sub trace { shift->_log('trace', @_) }
sub dump {
    my $self = shift;
    require Data::Dumper;
    my $dump = Data::Dumper::Dumper( @_);
    $dump =~ s/\$VAR1 = //;
    $self->_log('trace', $dump);
}
1;
