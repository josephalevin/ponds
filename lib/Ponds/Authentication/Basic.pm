package Ponds::Authentication::Basic;

use strict;
use warnings;

use Moo;
use MIME::Base64;
with 'Ponds::Authentication';

use Ponds qw( :context );

has callback => (
    is => 'rw',
    isa => sub { ref shift eq 'CODE' },
    required => 1,    
);

has realm => (
    is => 'rw'
);

sub authenticate {
    my ($self) = @_;
    
    my $auth = env('HTTP_AUTHORIZATION');

    # note the 'i' on the regex, as, according to RFC2617 this is a 
    # "case-insensitive token to identify the authentication scheme"
    if ($auth && $auth =~ /^Basic (.*)$/i) {
        my($user, $pass) = split /:/, (MIME::Base64::decode($1) || ":"), 2;
        $pass = '' unless defined $pass;

        if ($self->callback->($user, $pass)) {
            return 1; 
        }
        
    }

    die client_error ({
        status => 401,
        code   => 'authorization_required',        
    });

}

1;
