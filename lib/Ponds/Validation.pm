package Ponds::Validation;

use strict;
use warnings;

# other
use Module::Pluggable::Object;
use Exporter qw( import );
our @EXPORT_OK = qw(
    init_datatype_cache
    register_datatype
    register_datatype_package
    lookup_datatype
    validate
);



my $cache;

sub init_datatype_cache {
    $cache = {};

    register_datatype_package('Ponds::Validation');
}

sub register_datatype {
    my ($name, $type) = @_;

    init_datatype_cache() unless defined $cache;

    $cache->{$name} = $type;
}

sub register_datatype_package {
    my ($package) = @_;
    # load the resources into the worker_stash
    my $loader = Module::Pluggable::Object->new(
        search_path => $package,
        require     => 1,
#        instantiate => 'new',
    );

    for my $plugin ($loader->plugins){        
        die sprintf ('Package %s does now implement datatypes sub', ref $plugin) 
            unless $plugin->can('datatypes');

        my $types = $plugin->datatypes();
        next unless defined $types;

        for my $name (keys %$types){
            register_datatype ($name, $types->{$name});
        }
    }

}

sub lookup_datatype {
    my ($name) = @_;
    return unless $name;

    init_datatype_cache() unless defined $cache;

    return $cache->{$name};
}

sub validate {
    my ($args) = @_;
    
    my $type = $args->{type};
    my $datatype = lookup_datatype($type);
    die sprintf('unknown datatype: %s', $type) unless $datatype;
    my $condition = $datatype->{condition};

    my $value = $args->{value};
    # could preprocess here    
    
    my $ref = ref $condition;
    my $result;
    if($ref eq 'CODE'){
        $result = $condition->($args); 
    }
    elsif ($ref eq 'Regexp'){
        $result = $value =~ $condition;
    }
    else{
        die (sprintf 'unknown validation condition: %s', $ref);
    }
   
    unless ( $result ){
        # only set the text if the condition sub didn't already 
        $args->{text} ||= $datatype->{text};
    }

    return $result;
        
}


1;
