package Ponds::Context;

use strict;
use warnings;

use parent qw( Plack::Middleware );


# other
use Time::HiRes qw( time );

our (@ISA, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
    require Exporter;
    @ISA = qw( Exporter Plack::Middleware );
    @EXPORT_OK = qw(
        client_error
        client_errors
        client_warning
        client_warnings
        datatype
        env
        logger
        request
        resource
        response
        server_error
        server_errors
        server_warning
        server_warnings
        stash
        subject
        version
        wallclock
        wallclock_start
    );
    %EXPORT_TAGS = (
        all  => [ grep { $_ !~ /^_/} @EXPORT_OK ],
    );
}

my $_env;
my $logger;

# plack middleware call
sub call {
    # pre-processing $env
    my $time = time;
    my($self, $env) = @_;
    
    $_env = $env;
    env('ponds.stash', {});
    wallclock_start($time);
    my $res = $self->app->($env);
    # post-processing $res
    return $res;
}


sub env {
    die 'env not valid outside of request' unless $_env;
    return $_env if @_ == 0;
    my ($key, $value) = @_;    

    return $_env->{$key} if @_ == 1;

    $_env->{$key} = $value;
}

sub request {
    my $request = env->{'ponds.request'};
    unless ($request){
        require Ponds::Request;
        $request = Ponds::Request->new(env);
        env->{'ponds.request'} = $request;       
    }
    return $request;
}

sub response {
    my $response = env->{'ponds.response'};
    unless ($response){
        require Ponds::Response;
        $response = Ponds::Response->new();
        env->{'ponds.response'} = $response;      
    }
    return $response;
}

sub resource { stash ('resource', @_) };

sub stash {
    my ($key, $value) = @_;
    die sprintf('stash not valid outside of request: %s', $key) unless $_env;
    my $stash = $_env->{'ponds.stash'};

    return $stash if @_ == 0;
    return $stash->{$key} if @_ == 1;
    $stash->{$key} = $value;
}

# auth/authz subject, should be set by middleware handling auth checks
sub subject { stash ('subject', @_) };

sub logger {
    
    unless($logger){
        require Ponds::Logger;
        $logger = Ponds::Logger->new();
    }

    return $logger;
}

sub server_error  { _message('server_errors', @_) }
sub server_errors { _messages('server_errors') }

sub server_warning  { _message('server_warnings', @_) }
sub server_warnings { _messages('server_warnings') }

sub client_error  { _message('client_errors', @_) }
sub client_errors { _messages('client_errors') }

sub client_warning  { _message('client_warnings', @_) }
sub client_warnings { _messages('client_warnings') }

sub _messages {
    my ($type) = @_;
    my $msgs = stash($type);
    if (not defined $msgs){
        $msgs = []; 
        stash($type, $msgs);
    }
    return wantarray ? @$msgs : $msgs;
}

# shortcut to push a message on
sub _message {    
    my ($type, $msg) = @_;
#    _message($type, $_) for @$msg if ref($msg) eq 'ARRAY';
    $msg = {
        text => $msg,
    } unless ref $msg;
    push _messages($type), $msg;
    return $msg;
}

sub wallclock {
    my $time = time;
    my $wallclock_start = wallclock_start();
    return $time - $wallclock_start;
}

sub wallclock_start { stash ('wallclock_start', @_) };

1;
