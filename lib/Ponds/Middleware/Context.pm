package Ponds::Middleware::Context;

use strict;
use warnings;

use parent qw(Plack::Middleware);

sub call {
    my($self, $env) = @_;
    # pre-processing $env
    my $res = $self->app->($env);
    # post-processing $res
    return $res;
} 

1;
