package Ponds::Dispatcher::Resource;

use strict;
use warnings;

use Moo;
use Module::Pluggable::Object;
use Ponds qw( :context );
use Router::Simple;

# implements Role
with 'Ponds::Dispatcher';

has router => (
    is => 'ro',
    default => sub { Router::Simple->new() },
);

has packages => (
    is => 'ro',
    required => 1,
    isa => sub {die 'arrayref required' unless ref $_[0] eq 'ARRAY'}
);

sub BUILD {
    my ($self) = @_;

    # load the resources into the worker_stash
    my $loader = Module::Pluggable::Object->new(
        search_path => $self->packages,
        require     => 1,
        instantiate => 'new',
    );  

    for my $resource ($loader->plugins){
        printf "%s\n", ref $resource;
        die 'must implement Ponds::Resource' unless $resource->does('Ponds::Resource');
        $self->router->connect(
            $resource->route, 
            { 
                resource => $resource, 
            },{ 
                method => $resource->method,
            }
        );
        
        $resource->preload;
    }


}

sub dispatch {
    my ($self) = @_;

    my $route = $self->router->match(env);

    if( $route ) {
        my $resource = $route->{resource};
        resource($resource);
        my ($status, $result) = $resource->call();

        return ($status, $result);
    }
    # TODO: if the path ends in / then try redirecting

    client_error({
        status => 404,
        code   => 'not_found',
        text   => sprintf('%s %s not found',
                        env('REQUEST_METHOD'), 
                        env('PATH_INFO') || '/' 
                   ),});
    return (404);        
}

1;
