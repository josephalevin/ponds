package Ponds::Tools::FieldsParser;

use strict;
use warnings;

use Clone::Fast qw (clone);
use Ponds qw( :context );

#       empty string is treated like a single * (recursive wildcard), will return everything 
# *     recursive wildcard
# ()    select fields of the parent
# .     separates object.field is a '.' better than '/', I think so because the '.' is used in javascript
# ,     separates field strings
#
# EXAMPLES
# items(id,name,name_i18n,price_details(currency,price))

sub parse {
    my ($args) = @_;

    my $fields    = $args->{fields};
    my $template  = $args->{template};
    my $sep       = $args->{separator} || '.'; # default to dot separated fields
    
    my $sep_regex = $sep =~ /[\.\/]/ ? '\\'.$sep : $sep;
    my @matches = $fields =~ m/ [$sep_regex\(\)\*,] | [a-z0-9_]+/gix;
#    printf ("%s\n", Dumper \@matches);

    # final result
    my $result = {};

    # keep track of the current result context 
    my @stack = ( [$result, $template] );
    my $stack_dots = 0;

    # temp storage for the last field token seen
    my $last_field;

    # loop over each of the parsed tokens and take action
    for my $token ( @matches){
        my ($context, $tmp) = @{$stack[-1]}; # easier to refer to as context

        if ( $token eq '(' || $token eq $sep ) { # open group or dot field
            my $to_push = $context->{$last_field};
            unless (ref($to_push) eq 'HASH'){
                $to_push = {};
                $context->{$last_field} = $to_push;
            }
            
            # push the new context onto the stack
            push @stack, [$to_push, $tmp->{$last_field}];

            # keep track of how far down the dots take us so we can pop back up
            $stack_dots++ if $token eq $sep;

        }
        elsif ( $token eq ')' || $token eq ',') { # close group or next item
            # pop off the stack all the dot fields
            pop @stack for 1..$stack_dots;
            $stack_dots = 0;

            pop @stack if $token eq ')'; # close the group
        }
        elsif ( $token eq '*' ) { # wildcard, fill from the template
            my $clone = clone ($tmp);
            @$context{ keys %$clone } = @$clone{ keys %$clone };
        }
        elsif ($template && !$template->{$token}) { 
            # check if the token is valid against the template            
            client_error({
                status => 400,
                code   => 'invalid_field',
                field  => $token,                 
                text   => "$token is an invalid field", 
            });
        }
        else { # field, verify it is valid and allowed against the template            
            $context->{$token} = 1;
            $last_field = $token;
        }
    }

    return $result;

}

sub to_template {
    my ($args) = @_ ;

    my $meta = $args->{meta};

    my $template = {};

    my @stack = map {(ref $meta->{$_} eq 'HASH') 
        ? [$_, $meta, $template] : ()} keys %$meta;

    while ( my $tuplet = pop @stack){
        my ($key, $from, $to) = @$tuplet;
        
        my $pushed = 0; 
        for my $child_key (keys %{$from->{$key}}){
            if (ref $from->{$key}{$child_key} eq 'HASH'){
                $to->{$key}->{$child_key} = {};
                push @stack, [$child_key, $from->{$key}, $to->{$key}];
                $pushed++;
            }
        }
        $to->{$key} = 1 unless $pushed;
    }

    return $template;

}

sub filter {
    my ($args) = @_;

    my $result = $args->{result};
    my $template = $args->{template};

    my @stack = ([$template, $result]);

    while ( my $pair = pop @stack){
        my ($from, $to) = @$pair;

        for my $key (keys %$to){
            unless ( $from->{$key} ){
                delete $to->{$key};
                next;
            }
            if( ref $to->{$key} eq 'HASH'){
                push @stack, [$from->{$key}, $to->{$key}];
            }
            elsif( ref $to->{$key} eq 'ARRAY'){
                push @stack, map { (ref $_ eq 'HASH') ? [$from->{$key}, $_] : ()} @{$to->{$key}};
            }

        }
    }

    return $result;

}


1;
