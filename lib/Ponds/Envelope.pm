package Ponds::Envelope;

use strict;
use warnings;

use Moo::Role;

#requires 'adapt_request';
requires 'adapt_response';

1;
