package Ponds::Validation::Datatypes;

use strict;
use warnings;

sub datatypes {
    return {
        float   => {
            description => 'numerical float',
            condition   => qr/^[0-9]*\.?[0-9]*$/,
            text        => 'Value must be a float',
        },
    };
}

1;
