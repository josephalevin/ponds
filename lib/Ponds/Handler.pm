package Ponds::Handler;
use parent qw( Plack::Component );

use strict;
use warnings;

use Moo;
use Ponds::Context qw( :all );

has dispatcher => (
    is  => 'rw',
    isa => sub { _isa_role($_[0], 'Ponds::Dispatcher' ) },
);

has envelope => (
    is  => 'rw',
    isa => sub { _isa_role($_[0], 'Ponds::Envelope' ) },
    default => sub { _create_instance('Ponds::Envelope::Default') }
);

has transport => (
    is  => 'rw',
    isa => sub { _isa_role($_[0], 'Ponds::Transport' ) },
    default => sub { _create_instance('Ponds::Transport::JSON') }
);

has authentication => (
    is  => 'rw',
    isa => sub { _isa_role($_[0], 'Ponds::Authentication' ) },
);

# test if a reference implements a given role
sub _isa_role {
    my ( $ref, $role ) = @_;
    die "Reference to an implementation of $role is required." 
        #unless UNIVERSAL::isa( $ref, 'does' ) && $ref->does( $role );
        unless $ref->does( $role );
}

sub _create_instance {
    my ($class) = @_;
    (my $require = $class . ".pm") =~ s{::}{/}g;
    require $require;
    return $class->new();
}

sub prepare_app {
    my ($self) = @_;
    die 'dispatcher not set' unless $self->dispatcher;
}

sub call {
    my ( $self, $env ) = @_;
    
    my $auth       = $self->authentication;
    my $transport  = $self->transport;
    my $dispatcher = $self->dispatcher;
    my $envelope   = $self->envelope;

    # check if we can route this request to anything
    my ($status, $result);
    eval{
        if ($auth){
            logger->trace('authentication');
            $auth->authenticate();
            logger->trace('authentication done');
        }

        logger->trace('dispatching request');
        ($status, $result) = $dispatcher->dispatch();
        logger->trace('finished dispatching result');

    } or do {
        # try and find a status code from the server and client errors
        logger->error('died while dispatching: %s', $@);
        my @all_errors = sort { ($b->{status} || 0) <=> ($a->{status} || 0)} 
            (server_errors(), client_errors());
        $status = $all_errors[0]->{status} if $all_errors[0];

        # don't have any errors pushed, going to give a debug
        unless (@all_errors){
            server_error({
                status  => 500, # not really needed, but nice for the client
                code    => 'unknown',
                text   => $@,
            });
        }
    };
    
    $status ||= 500;    # status defaults to 500 when undefined
    response->status($status);

    # if there is an envelope, wrap the body
    if ( $envelope ) {
        $result = $envelope->adapt_response($result);
    }
    
    #printf "%s\n", Data::Dumper::Dumper( $result );

    my $output = $transport->write($result);    

# can't clear yet because the transport might use it while writing    
#    _set_env(undef);    # clear the env at the end of each request to prevent carryover
    return $output;

}

1;
