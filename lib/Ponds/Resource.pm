package Ponds::Resource;

use strict;
use warnings;

#ponds
use Ponds::Validation qw( validate );
use Ponds::Tools::FieldsParser;

#other
use Moo::Role;
use Ponds qw( :context );
use Data::Dumper;


has config => (
    is      => 'ro',
    lazy    => 1,
    builder => '_resource',
);

has input => (
    is      => 'ro',
    default => sub { shift->config->{input} },
);

has output => (
    is      => 'ro',
    default => sub { shift->config->{output} },
);


has route => (
    is      => 'ro',
    default => sub { shift->config->{route} },
);

has method => (
    is      => 'ro',
    default => sub { shift->config->{method} },
);

# lazy build the meta
requires '_resource';

sub preload {
    my ($self) = @_;
    $self->output_fields_template;
}

sub output_fields_template {
    my ($self) = @_;

    return $self->{output_fields_template} if $self->{output_fields_template};
    
    my $template = Ponds::Tools::FieldsParser::to_template({
        meta => $self->output,
    });

    $self->{output_fields_template} = $template;
    
    return $template;   
}

sub call {
    my ($self) = @_;

    logger->trace('before_process');
    eval {
        $self->before_process();
        logger->trace('before_process done');
        1;
    } or do {
        logger->trace('before_process failed');        
        die $@; # die again
    };
    
    # check if any errors were pushed
    die 'before_process generated errors' if @{server_errors()} || @{client_errors()};
     
    my ($status, $result);
    my $process_method = 'process_' . lc request->method();
    if ( $self->can($process_method) ){
        logger->trace($process_method);
        eval {
            ($status, $result) = $self->$process_method();
            logger->trace('%s done', $process_method);
            1;
        } or do {
            logger->trace('%s failed', $process_method);
            die $@; # die again
        };
    } else {
        logger->trace('process');
        eval {
            ($status, $result) = $self->process();
            logger->trace('process done');
            1;
        } or do {
            logger->trace('process failed');
            die $@; # die again
        };
    }

    logger->trace('after_process');
    eval {
        $result = $self->after_process($result);
        logger->trace('after_process done');
        1;
    } or do {
        logger->trace('after_process failed');
        die $@; # die again
    };
    
    # actually made, phew...
    return $status, $result;
}

sub before_process {
    my ($self) = @_;

    logger->trace('check_input');
    $self->check_input();
    logger->trace('check_input done');
}

sub check_input {
    my ($self) = @_;

    my $fields_string = request->param('fields');
    
    if ($fields_string){
        my $fields = Ponds::Tools::FieldsParser::parse({
            fields   => $fields_string,
            template => $self->output_fields_template,
        });
        request->fields($fields);
    }
    else{
        # everything by default
        request->fields($self->output_fields_template);
    }
    
    # hash of required inputs not yet seen 
    my $required_inputs = { map { $self->input->{$_}->{required} ? ($_ => 1) : () } 
        keys %{$self->input} };
    
    # loop over each param and validate
    for my $name (request->parameters->keys){
        next unless $name;
        logger->trace('check_input:%s', $name);

        my $input = $self->input->{$name};

        # warning for each unknown input
        unless ( $input ){
            logger->trace('%s unknown', $name);
            client_warning({
                code   => 'input_unknown',
                input  => $name,
                text   => "$name is not a recognized input",
            });
            next; # not recognized, so we can skip other checks
        }        
        
        my $type = $input->{type};
        next if defined $type && $type eq 'fields';
         
        my $args = { 
            type  => $type,
            value => request->param($name),

        };
        if(validate($args)){
            # todo, should set the value back to the parameters if it changed
        }
        else{
            # invalid input
            logger->trace('%s invalid: %s', $name, $args->{text} | 'unknown');
            client_error({
                status => 400,
                code   => 'input_validation',
                input  => $name,
                text   => $args->{text} || 'unknown validation error',
            });
        }

        # todo check for conflicts
        # todo check for depends
        
        # remove required because we've seen this param
        delete $required_inputs->{$name} if $required_inputs->{$name};
    }
    

    # error for each unseen required input
    for my $required (keys %$required_inputs) {
        logger->trace('%s missing', $required);
        client_error({
            status => 400,
            code   => 'input_required',
            input  => $required,
            text   => "$required is a required input",
        });
    }
    
    
}

sub process {
    die server_error({
        status => 500,
        text   => 'process not yet implemented...'
    });
}

sub after_process {
    my ($self, $result) = @_;

    logger->trace('filter_output');
    $result = $self->filter_output($result);
    logger->trace('filter_output done');

    logger->trace('collapse_output');
    $result = $self->collapse_output($result) if request->param('fields');
    logger->trace('collapse_output done');
    return $result;
}

sub filter_output {
    my ($self, $result) = @_;

    return Ponds::Tools::FieldsParser::filter({
        result   => $result,
        template => request->fields,
    });

}

sub collapse_output {
    my ($self, $result) = @_;
    
    if (ref $result eq 'HASH'){
        for my $key (keys %$result){
            my $value = $result->{$key};
            if (not $value){
                delete $result->{$key};
                next;
            }
            $value = $self->collapse_output($value);
            if (not $value){
                delete $result->{$key};
                next;
            }
            $result->{$key} = $value;
        }
        # collapse to the first value if there is only one
        my @values = values %$result;
        return $values[0] if @values == 1;
    }
    elsif (ref $result eq 'ARRAY' ){
        # arrays remain arrays, but we can collapse what's inside
        for (my $i = 0; $i < @$result; $i++){
            $result->[$i] = $self->collapse_output($result->[$i]);
        }
    }
  
    # fallback if not collapsed 
    return $result;
}






1;
