package Ponds::Transport;

use strict;
use warnings;

use Moo::Role;


has envelope => (
    is      => 'rw',
);

# read the request
requires 'read';

# write the response 
requires 'write';

1;
