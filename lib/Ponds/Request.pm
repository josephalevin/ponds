package Ponds::Request;

use strict;
use warnings;

use parent qw( Plack::Request );

use Ponds::Context qw( stash ); # don't import env, Plack::Request has its own

sub field {    
    my ($self, $field) = @_;
    return $self->fields()->{$field} if defined fields();
}

sub fields {
    my $self = shift;
    return stash('request_fields', @_);
}

1;
