package Ponds::Response;

use strict;
use warnings;

use parent qw( Plack::Response );

sub output_headers {
    my ($self) = @_;
    my $headers = [];

    $self->headers->scan(sub {
        push @$headers, @_;
    });

    return $headers;
}


1;
