package Ponds::Envelope::Default;

use strict;
use warnings;

use Moo;
use Ponds qw( :context );

with 'Ponds::Envelope';

sub adapt_response {
    my ($self, $result) = @_;

    my $output = {};

    $output->{result} = $result if $result;

    $output->{exceptions} = server_errors if @{server_errors()} > 0;
    $output->{errors} = client_errors if @{client_errors()} > 0;
    $output->{warnings} = client_warnings if @{client_warnings()} > 0;

    return $output;
}


1;
