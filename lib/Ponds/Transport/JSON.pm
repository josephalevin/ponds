package Ponds::Transport::JSON;

use strict;
use warnings;

use Moo;
# implements role
with 'Ponds::Transport';

use JSON;
use Ponds qw( :context );

use Data::Dumper;

has json => (
    is => 'rw',
    default => sub { JSON->new->utf8->allow_nonref },
);

sub read {
    my ( $self, $args ) = @_;

    my $req    = $args->{request};
}

sub write {
    my ( $self, $result ) = @_;

    response->content_type('application/json');
    
    return sub {
        my $responder = shift;
        my $writer = $responder->([ response->status, response->output_headers ]);
            eval {
#                $writer->write($self->json->encode($result));a
                 $self->stream($writer, $result);
            } or do {
                printf $@;
                # unable to stream the json
            };
            $writer->close();
      };

}

sub stream {
    my ($self, $writer, $value) = @_;
    my $ref = ref $value;
    if ($ref eq 'HASH'){
        $writer->write('{');
        my $i = 0;
        # TODO: sort the keys by streaming priority, errors and warnings last
        for my $key (sort keys %$value){
            $writer->write(',') unless $i++ == 0;

            $writer->write($self->json->encode($key));
            $writer->write(':');
            $self->stream($writer, $value->{$key}); 

        }
        $writer->write('}');
    }
    elsif ($ref eq 'ARRAY'){
        $writer->write('[');
        my $i = 0;
        for my $element (@$value){
            $writer->write(',') unless $i++ == 0;

            $self->stream($writer, $element);
        }
        $writer->write(']');
    }
    elsif ($ref eq 'CODE'){
        # delayed lookup as an array
        $writer->write('[');
        my $i = 0;
        while ( defined (my $element = &$value($i)) ){
            $writer->write(',') unless $i++ == 0;

            # could just stream the whole json->encode from this point down
            # do we really need to support nested coderefs?
            $self->stream($writer, $element );
        }
        $writer->write(']');
    }
    else {
       $writer->write($self->json->encode($value)); 
    }
    
}

1;
